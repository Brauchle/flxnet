#include "flxnet.h"

FlxNet::FlxNet(){
  in = 0;
  out = 0;
  ref = 0;
}
FlxNet::FlxNet(int m_in, std::vector<int> m_neu){
  in = 0;
  out = 0;
  ref = 0;
  init(m_in, m_neu);
}

FlxNet::~FlxNet(){
  for (size_t i = 0; i < n_neurons.size()+1; i++) {
    delete[] buffer[i];
  }

  delete[] ref;
}

float FlxNet::train(){
// trains the neural network as long as data is true (should return false when all data is used up or a limit is reached);
  while (data.next()) {
    forward();
    backward();
    update();
  }
  return loss();
}

float FlxNet::train(int limit){
// trains the neural network as long as data is true (should return false when all data is used up or a limit is reached);
// sets a limit instead of using the default limit
  data.set_limit(limit);
  train();
  return loss();
}

void FlxNet::forward(){
// calculates the ouput for each neuron on a layer by layer basis
  for (size_t i = 0; i < neurons.size(); i++) {
    for (size_t j = 0; j < neurons[i].size(); j++) {
      neurons[i][j].calc_out();
    }
  }
}

void FlxNet::backward(){
// calculates the weight changed based on the Loss-Function
  int layer_cnt = neurons.size()-1;
  // for each output node : dLoss/d_out_n
  int i = 1;
  for (size_t i = 0; i < neurons[layer_cnt].size(); i++) {
    neurons[layer_cnt][i].add_error(d_loss_out(i));
    neurons[layer_cnt][i].calc_error();
    neurons[layer_cnt][i].calc_changes(lf);
  }

  layer_cnt--;
  for (int i = layer_cnt; i > -1; i--) {
    for (size_t j = 0; j < neurons[i].size(); j++) {
      // propagate errors
      for (size_t k = 0; k < neurons[i+1].size(); k++) {
        neurons[i][j].add_error(neurons[i+1][k].get_error(j));
      }
      // after error collection
      neurons[i][j].calc_error();
      // calc weight change for batch updates
      neurons[i][j].calc_changes(lf);
      // update or not update
    }
  }
}

void FlxNet::update(){
// discussion about when to update, every training sequence/data point or after a complete run through the training data set
// online vs batch learning, apparently both converge but take different paths
  for (size_t i = 0; i < neurons.size(); i++) {
    for (size_t j = 0; j < neurons[i].size(); j++) {
      neurons[i][j].update();
    }
  }
}


float FlxNet::loss(){
// Loss Function, may implement others at some point
// exemplary ms error
  float error = 0;
  for (size_t i = 0; i < n_out; i++) {
    error += (ref[i]-out[i])*(ref[i]-out[i]);
  }
  return error/n_out;
}

float FlxNet::d_loss_out(int n){
  return (2.0/n_out)*(out[n]-ref[n]);
}


void FlxNet::show_structure(){
  std::cout << "#FlxNet" << std::endl;
  std::cout << "Inputs:  " << n_in << std::endl;
  std::cout << "Layers:  " << n_neurons.size() << std::endl;
  std::cout << "Outputs: " << n_out << std::endl << std::endl;

  for (size_t i = 0; i < n_neurons.size(); i++) {
    std::cout << "Layer " << i + 1 << std::endl;
    for (size_t j = 0; j < neurons[i].size(); j++) {
      std::cout << "   " << neurons[i][j] << std::endl;
    }
  }
}

void FlxNet::classify(std::vector<float> data){
  std::cout << "Input Data: ";
  for (size_t i = 0; i < n_in; i++) {
    in[i] = data[i];
    std::cout << in[i] << " ";
  }
  std::cout << std::endl;

  forward();

  std::cout << "Decision  : ";
  for (size_t i = 0; i < n_out; i++) {
    std::cout << out[i] << " ";
  }
  std::cout << std::endl;
}

void FlxNet::save(std::string filename){
  std::cout << "#Save NN ";
  std::ofstream save_file;
  save_file.open(filename.c_str(), std::fstream::binary | std::fstream::trunc);

  // Inputs   int
  save_file.write(reinterpret_cast<char *>(&n_in), sizeof(n_in));

  // N-layers int
  int temp = n_neurons.size();
  save_file.write(reinterpret_cast<char *>(&temp), sizeof(temp));
  // Neurons per Layer int
  for (size_t i = 0; i < n_neurons.size(); i++) {
    temp = n_neurons[i];
    save_file.write(reinterpret_cast<char *>(&temp), sizeof(temp));
  }

  // Weights L1 N1 -> LN NN float
  std::vector<int>n_weights;
  n_weights.push_back(n_in+1);
  for (size_t i = 0; i < n_neurons.size()-1; i++) {
    n_weights.push_back(n_neurons[i]+1);
  }

  float f_temp;
  for (size_t i = 0; i < neurons.size(); i++) {
    for (size_t j = 0; j < neurons[i].size(); j++) {
      for (size_t k = 0; k < n_weights[i]; k++) {
        f_temp = neurons[i][j].get_weight(k);
        save_file.write(reinterpret_cast<char *>(&f_temp), sizeof(f_temp));
      }
    }
  }
  save_file.close();
  std::cout << "+" << std::endl;
}

bool FlxNet::load(std::string filename){
  std::cout << "#Loading Save ";
  std::ifstream save_file;
  save_file.open(filename.c_str(), std::fstream::binary);

  if (!save_file){
    return false;
  }

  save_file.seekg(0, save_file.end);
  int len = save_file.tellg();
  save_file.seekg(0, save_file.beg);

  char *file_buffer = new char[len];
  char *file_pos    = file_buffer;
  save_file.read(file_buffer,len);

  save_file.close();


  // parse file
  // Inputs   int
  int f_inputs = *reinterpret_cast<int *>(file_pos);
  file_pos = file_pos + sizeof(f_inputs);

  // N-layers int
  int f_layers = *reinterpret_cast<int *>(file_pos);
  file_pos = file_pos + sizeof(f_layers);

  // Neurons per Layer int
  int temp;
  std::vector<int> f_neurons;
  for (size_t i = 0; i < f_layers; i++) {
    temp = *reinterpret_cast<int *>(file_pos);
    file_pos = file_pos + sizeof(temp);
    f_neurons.push_back(temp);
  }

  // Weights L1 N1 -> LN NN float
  std::vector<int>f_n_weights;
  f_n_weights.push_back(f_inputs+1);
  for (size_t i = 0; i < f_neurons.size()-1; i++) {
    f_n_weights.push_back(f_neurons[i]+1);
  }

  std::vector< std::vector<float> >f_weights;
  float f_temp;
  for (size_t i = 0; i < f_layers; i++) {
    for (size_t j = 0; j < f_neurons[i]; j++) {
      std::vector<float> temp_vec;
      for (size_t k = 0; k < f_n_weights[i]; k++) {
        temp_vec.push_back( *reinterpret_cast<float *>(file_pos) );
        file_pos = file_pos + sizeof(f_temp);
      }
      f_weights.push_back(temp_vec);
    }
  }

  delete[] file_buffer;
  std::cout << "+" << std::endl;

  // Initialize Network
  clean();
  init(f_inputs, f_neurons);

  // set Weights
  int neu_cnt = 0;
  for (size_t i = 0; i < n_neurons.size(); i++) {
    for (size_t j = 0; j < neurons[i].size(); j++) {
      neurons[i][j].set_weights(f_weights[neu_cnt++]);
    }
  }
  return true;
}

void FlxNet::clean(){
  std::cout << "Clean Up ";
  for (size_t i = 0; i < buffer.size(); i++) {
    delete[] buffer[i];
  }
  delete[] ref;
  in  = 0;
  out = 0;
  ref = 0;
  n_neurons.clear();
  neurons.clear();
  buffer.clear();
  buffer_size.clear();
  std::cout << "+" << std::endl;
}

void FlxNet::new_nn(int m_in, std::vector<int> m_neurons){
  clean();
  init(m_in, m_neurons);
}

void FlxNet::new_nn(){
  float temp;
  int layers;
  int m_in;
  std::vector<int> m_neurons;
  std::cout << "#Generating a new Network" << std::endl;
  std::cout << "Input Dimension: ";
  std::cin >> m_in;
  std::cout << "Number of Layers: ";
  std::cin >> layers;
  for (size_t i = 0; i < layers; i++) {
    std::cout << "Neurons Layer " << i+1 << ": ";
    std::cin >> temp;
    m_neurons.push_back(temp);
  }


  clean();
  init(m_in, m_neurons);

}

void FlxNet::init(int m_in, std::vector<int> m_neurons){
  std::cout<< "#Init ";
  n_in = m_in;
  n_neurons = m_neurons;
  buffer.push_back(new float[n_in]);
  in = buffer[0];
  buffer_size.push_back(n_in);
  for (size_t i = 0; i < n_neurons.size(); i++) {
    buffer.push_back(new float[n_neurons[i]]);
    buffer_size.push_back(n_neurons[i]);
  }
  out   = buffer[buffer.size()-1];

  n_out = n_neurons[n_neurons.size()-1];
  n_ref = n_out;
  ref = new float[n_out];
  data.init(in, ref, n_in, n_out);

  for (size_t i = 0; i < n_neurons.size(); i++) {
    // Input Variables are all elements of buffer[i]
    // The single output gets stored in buffer[i+1]+i for the i-th neuron in each layer
    std::vector<Neuron> temp_vec;

    float inputs;
    if (i==0) {
      inputs = n_in;
    } else {
      inputs = n_neurons[i-1];
    }

    for (size_t j = 0; j < n_neurons[i]; j++) {
      temp_vec.push_back(Neuron(inputs, buffer[i], buffer[i+1]+j));
    }
    neurons.push_back(temp_vec);
  }
  std::cout<< "+"<< std::endl;
}
