#ifndef __FLXNET_DATA__
#define __FLXNET_DATA__

#include <iostream>
#include <iomanip> // same
#include <random> // random initializer
#include <cmath> // exp

#include "functions.h"

class T_Data{
private:
  float *in;
  float *ref;
  float n_in, n_out;

  int count, limit;
public:
  T_Data();
  ~T_Data();

  void init(float *, float *, int, int);
  bool generate_training_data();
  bool next();

  void set_limit(int);
};

#endif
