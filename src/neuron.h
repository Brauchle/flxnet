#ifndef __FLXNET_NEURON__
#define __FLXNET_NEURON__

#include <iostream>
#include <iomanip> // same
#include <random> // random initializer
#include <cmath> // exp

#include "functions.h"

class Neuron{
private:
  int n_in;
  float *weights;
  float *bias;
  float *in;
  float *out;
  friend std::ostream & operator<<(std::ostream &, const Neuron&);

  // learning
  float neuron_res;
  float d_loss_neu;
  float d_loss_b;
  float *d_loss_w;

  // Propagation
  float d_loss_out;

  // Memory
  int n_changes;
  float *w_change;

public:
  Neuron(int, float*, float*);
  Neuron(const Neuron &);
  ~Neuron();

  void  calc_out();

  float get_error(int);
  void  add_error(float);
  void  calc_error();
  void  calc_changes(float);
  void  update();

  void set_weights(std::vector<float>);

  int   get_n_weights();
  float get_weight(int);
};

#endif
