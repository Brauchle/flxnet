#include "data.h"

T_Data::T_Data(){limit = 10;}
T_Data::~T_Data(){}


void T_Data::init(float *m_in, float *m_ref, int m_nin,  int m_nout){
  in = m_in;
  ref = m_ref;
  n_in = m_nin;
  n_out = m_nout;
  count = 0;
}


bool T_Data::next(){
  return generate_training_data();
}

bool T_Data::generate_training_data(){
  if (count < limit) {
    std::random_device seed;
    std::mt19937 generator(seed()); //mersenne_twister_engine
    std::uniform_real_distribution<float> distribution2(0,0.5);
    std::uniform_real_distribution<float> distribution1(0,1);

    for (size_t i = 0; i < n_out; i++) {
      ref[i] = round(distribution1(generator));
    }

    for (size_t i = 0; i < n_in + 1; i++) {
      in[i] = ref[i]*0.5 + distribution2(generator);
    }

      count++;
      return true;
  }

  count = 0;
  return false;
}

void T_Data::set_limit(int m_limit){limit = m_limit;}
