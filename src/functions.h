#ifndef __FLXNET_FUNCTIONS__
#define __FLXNET_FUNCTIONS__

#include <cmath> // exp


float sigmoid(float x);
float d_sigmoid(float x);

#endif
