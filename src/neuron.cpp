#include "neuron.h"

Neuron::Neuron(int n_inputs, float * buffer_in, float* buffer_out){
  n_in = n_inputs;
  in =  buffer_in;
  out = buffer_out;

  n_changes = 0;

  weights  = new float[n_in + 1];
  w_change = new float[n_in + 1];

  d_loss_w = new float[n_in];
  bias     = &weights[n_in];

  std::random_device seed;
  std::mt19937 generator(seed()); //mersenne_twister_engine
  //std::default_random_engine generator;
  std::uniform_real_distribution<float> distribution(0.0,1.0);

  for (size_t i = 0; i < n_in + 1; i++) {
    weights[i]  = distribution(generator);
    w_change[i] = 0;
  }
}

Neuron::~Neuron(){
  delete[] weights;
  delete[] w_change;
  delete[] d_loss_w;
}

Neuron::Neuron(const Neuron &other){//cpy constructor
  n_in = other.n_in;
  in   = other.in;
  out  = other.out;

  n_changes = other.n_changes;

  weights = new float[n_in + 1];
  w_change= new float[n_in + 1];
  for (size_t i = 0; i < n_in+1; i++) {
    weights[i] = other.weights[i];
    w_change[i]= other.w_change[i];
  }

  bias     = &weights[n_in];

  d_loss_w = new float[n_in];
  for (size_t i = 0; i < n_in; i++) {
    d_loss_w[i] = other.d_loss_w[i];
  }
}

void Neuron::calc_out(){
  neuron_res = weights[n_in];
  for (size_t i = 0; i < n_in; i++) {
    neuron_res += weights[i] * in[i];
  }
  // activation
  *out = sigmoid(neuron_res);
}

void Neuron::add_error(float p_d_loss_out){
  d_loss_out += p_d_loss_out;
}

void Neuron::calc_error(){
  //d_loss/w_n = dloss/dout * dout/dneu * dneu/dw_n
  //d_loss/in  = dloss/dout * dout/dneu * dneu/din_n
  d_loss_neu = d_loss_out * d_sigmoid(out[0]);
  d_loss_out = 0;

  //for each weight calc dldw
  for (size_t i = 0; i < n_in; i++) {
    d_loss_w[i] = in[i] * d_loss_neu;
  }
  d_loss_b = 1.0 * d_loss_neu;
}

void Neuron::calc_changes(float lf){
  n_changes++;
  for (size_t i = 0; i < n_in; i++) {
    w_change[i] += -1* lf * d_loss_w[i];
    d_loss_w[i] = 0;
  }
  w_change[n_in] += -1 * lf * d_loss_b;
  d_loss_b = 0;
}

float Neuron::get_error(int pin_n){
  return d_loss_neu * weights[pin_n];
}


void Neuron::update(){
  for (size_t i = 0; i < n_in+1; i++) {
    weights[i] = weights[i] + w_change[i] / n_changes;
    w_change[i] = 0;
  }
  n_changes = 0;
}

std::ostream & operator<<(std::ostream &os, const Neuron& neu)
{
    os << "Neuron: ";
    for (size_t i = 0; i < neu.n_in+1; i++) {
      os << std::fixed << std::setw(11) << std::setprecision(7) << neu.weights[i] << " ";
    }
    // os << std::endl << "   In: " << neu.in << " Out: " << neu.out;
    return os;
}

int   Neuron::get_n_weights(){return n_in+1;}
float Neuron::get_weight(int n){return weights[n];}

void Neuron::set_weights(std::vector<float> n_we){
  for (size_t i = 0; i < n_in+1; i++) {
    weights[i] = n_we[i];
  }
}
