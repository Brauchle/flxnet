#ifndef __FLXNET__
#define __FLXNET__

#include <cmath> // exp
//#include <numeric>
#include <vector>
#include <array>
#include <iostream> // for debugging purposes
#include <iomanip> // same
#include <random>
#include <fstream> // to save and load
#include <string>

#include "neuron.h"
#include "functions.h"
#include "data.h"

class FlxNet {
private:
  float lf=0.1; //learning_factor

  int n_in=3;
  float *in;

  int n_out;
  float *out;

  int n_ref;
  float *ref;

  /* data */
  std::vector<int> n_neurons;
  std::vector<std::vector<Neuron> > neurons; //stores the neurons for each layer
  std::vector<float *>buffer; // stores the in/output layers of each layer
  std::vector<int> buffer_size;

  // Training data
  T_Data data;


public:
  FlxNet();
  FlxNet(int, std::vector<int>);
  virtual ~FlxNet();
  void init(int, std::vector<int>);

  // Basic Functions
  void new_nn();
  void new_nn(int, std::vector<int>);
  float train();
  float train(int);

  // when done training
  void classify(std::vector<float>);

  // Internal functions
  void forward();
  void backward();
  void update();

  // Loss Funcion
  float loss();
  float d_loss_out(int);

  // Activation Function -> Add here for easy switchability

  // console output that shows layers neurons and weights
  void show_structure();

  // save & load
  void save(std::string);
  bool load(std::string);
  void clean();
};

#endif
