#include <iostream>
#include "flxnet.h"

int main(int argc, char const *argv[]) {
  // int inputs;
  // std::vector<int> neurons;
  // if (argc > 1) {
  //   inputs = int(argv[1][0]-'0');
  //   for (size_t i = 2; i < argc; i++) {
  //     neurons.push_back(int(argv[i][0]-'0'));
  //   }
  // }
  // FlxNet testNet(inputs, neurons);

  FlxNet testNet;
  std::cout << "DEB" << std::endl;
  if ( !testNet.load("testfile.txt") ) {
    std::cout << "DEB" << std::endl;
    testNet.new_nn();
    std::cout << "DEB" << std::endl;
  }
  testNet.show_structure();

  bool bool_in=true;
  while (bool_in) {
    std::cout<< "How many training loops (0 to skip)? ";
    int int_in;
    std::cin>>int_in;
    if (int_in != 0) {
      std::cout << "Loss in last loop: " << testNet.train(int_in) << std::endl;
    }else{
      bool_in = false;
    }
  }

  testNet.classify( {1,0,1,0,1,1,1} );
  std::cout << std::endl;

  testNet.save("testfile.txt");

  return 0;
}
