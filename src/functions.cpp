#include "functions.h"

float sigmoid(float z){
  return (1.0 / (1.0 + exp(-1.0 * z)));
}

float d_sigmoid(float z){
//Derrivative of the Sigmoid Function
  return (z * (1.0 - z));
}
